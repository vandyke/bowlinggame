package test;

import static org.junit.Assert.*;
import org.junit.Test;
import main.BowlingGameMainClass;

public class MainClassTest {

    private String message, answer;

    /**
     * (When scoring "X" indicates a strike, "/" indicates a spare, "-"
     * indicates a miss) X X X X X X X X X X X X (12 rolls: 12 strikes) = 10
     * frames * 30 points = 300 9- 9- 9- 9- 9- 9- 9- 9- 9- 9- (20 rolls: 10
     * pairs of 9 and miss) = 10 frames * 9 points = 90 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/
     * 5/ 5/5 (21 rolls: 10 pairs of 5 and spare, with a final 5) = 10 frames *
     * 15 points = 150
     */

    private void assertScore() {

        assertEquals(answer, new BowlingGameMainClass().computeScore(message));
    }

    @Test
    public void testAllStrikes() {
        message = "x x x x x x x x x x x x";
        answer = "Score: 300, Strikes: 12, Spare: 0";
        assertScore();
    }

    @Test
    public void testNoSpareNoStrikes() {
        message = "9- 9- 9- 9- 9- 9- 9- 9- 9- 9-";
        answer = "Score: 90, Strikes: 0, Spare: 0";
        assertScore();
    }

    @Test
    public void testAllSpare() {
        message = "5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/5";
        answer = "Score: 150, Strikes: 0, Spare: 10";
        assertScore();
    }

    @Test
    public void testEndWithSpareAndStrike() {
        message = "63 7- x 53 x 5/ 72 2- x 5/x";
        answer = "Score: 130, Strikes: 4, Spare: 2";
        assertScore();
    }

    @Test
    public void testEndWithSpareAndMiss() {
        message = "63 7- x 53 x 5/ 72 2- x 5/-";
        answer = "Score: 120, Strikes: 3, Spare: 2";
        assertScore();
    }

    @Test
    public void testEndNormal() {
        message = "63 7- x 53 x 5/ 72 2- x 54";
        answer = "Score: 118, Strikes: 3, Spare: 1";
        assertScore();
    }

    @Test
    public void testWrongInput() {
        message = "          ";
        answer = "falsche Eingabe!";
        assertScore();
    }

    @Test
    public void testEmptyString() {
        message = "";
        answer = "falsche Eingabe!";
        assertScore();
    }

    @Test
    public void testPartlyScoreWithSpare() {
        message = "x 5/ x";
        answer = "Score: 40, Strikes: 2, Spare: 1";
        assertScore();
    }

    @Test
    public void testPartlyScoreWithStrikes() {
        message = "x x x";
        answer = "Score: 30, Strikes: 3, Spare: 0";
        assertScore();
    }

    @Test
    public void testPartlyWithNormal() {
        message = "52 71";
        answer = "Score: 15, Strikes: 0, Spare: 0";
        assertScore();
    }

    @Test
    public void testPartlyWithSpare() {
        message = "5/ 42";
        answer = "Score: 20, Strikes: 0, Spare: 1";
        assertScore();
    }

    @Test
    public void testPartlyWithSpareEnd() {
        message = "53 5/";
        answer = "Score: 8, Strikes: 0, Spare: 1";
        assertScore();
    }
}
