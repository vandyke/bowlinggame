package main;

public class BowlingGameMainClass {

    private int[] frameScore = new int[10];
    private final String wrongInput = "falsche Eingabe!";

    /**
     * Rechnet anhand der angegebenen Wuerfen das Spielergebnis aus
     *
     * @param str Ihre Wuerfe
     * @return das Spielergebnis
     */
    public String computeScore(String str) {
        frameScore = new int[10];

        if ("".equals(str)) {
            return wrongInput;
        }

        return getAnswer(countScore(str.split(" ")), countStrike(str), countSpare(str));
    }

    /**
     * Setzt den String zusammen und liefert das Spielergebnis nach dem Format:
     * "Score: 120, Strikes: 3, Spare: 2" zurueck
     *
     * @param score  Anzahl an Punkten
     * @param strike Anzahl an Strikes
     * @param spare  Anzahl an Spares
     * @return String mit dem Spielergebnis
     */
    private String getAnswer(int score, int strike, int spare) {
        StringBuilder sb = new StringBuilder();

        if (score == -1) {
            return wrongInput;
        }

        sb.append("Score: ");
        sb.append(score);
        addSeparator(sb);
        sb.append("Strikes: ");
        sb.append(strike);
        addSeparator(sb);
        sb.append("Spare: ");
        sb.append(spare);

        return sb.toString();
    }

    private void addSeparator(StringBuilder sb) {

        sb.append(", ");
    }

    private int countStrike(String str) {

        return countCharsInString("x", str);
    }

    private int countSpare(String str) {

        return countCharsInString("/", str);
    }

    /**
     * Zaehlt die Anzahl an gesuchten Chars im String
     *
     * @param charToCount ist das gesuchte Char/String
     * @param str         ist der String in dem die Anzahl der Chars gezaehlt wird
     * @return Anzahl an gesuchten Chars im String
     */
    private int countCharsInString(String charToCount, String str) {

        return str.length() - str.replace(charToCount, "").length();
    }

    private int countScore(String[] stringArray) {

        if (!isValidInputFormat(stringArray)) {
            return -1;
        }
        computeFrameScore(validateScore(stringArray));

        return frameScore[frameScore.length - 1];
    }

    private void printFrameScore() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < frameScore.length - 1; i++) {
            sb.append(frameScore[i]);
            addSeparator(sb);
        }
        sb.append(frameScore[frameScore.length - 1]);

        System.out.println("Your frame score: " + sb.toString());
    }

    /**
     * Berechnet und speichert den Punktestand pro Frame
     *
     * @param validateScore die vereinfachte bowling line
     */
    private void computeFrameScore(String validateScore) {

        for (int i = 0; i < frameScore.length; i++) {
            if (validateScore.isEmpty()) {
                fillRestOfArrayWithLastDigit(i);
                break;
            }
            if (validateScore.charAt(0) == 'x') {
                // Strike -> die naechsten 2 Nachbarn dazu addieren
                if (validateScore.length() >= 3) {
                    frameScore[i] = getSumFromTwoNeighbors(validateScore);
                    validateScore = validateScore.substring(1);
                }
            } else {
                int sum = getSumWithNeighbor(validateScore);
                if (sum == 10) {
                    // es handelt sich hier um Spare
                    if (validateScore.length() <= 2) {
                        fillRestOfArrayWithLastDigit(i);
                        break;
                    }
                    if (validateScore.charAt(2) == 'x') {
                        sum += 10;
                    } else {
                        sum = sum + Character.getNumericValue(validateScore.charAt(2));
                    }
                }
                frameScore[i] = sum;
                validateScore = validateScore.substring(2);
            }
            if (i != 0) {
                frameScore[i] += frameScore[i - 1];
            }
        }
    }

    /**
     * Fuellt den Framescore ab dem angegebenen Index aus
     *
     * @param i ab dem Index
     */
    private void fillRestOfArrayWithLastDigit(int i) {
        for (int j = i; j < frameScore.length; j++) {
            frameScore[j] = frameScore[i - 1];
        }
    }

    private int getSumWithNeighbor(String str) {

        return Character.getNumericValue(str.charAt(0)) + Character.getNumericValue(str.charAt(1));
    }

    private int getSumFromTwoNeighbors(String str) {
        int value = 0;

        for (int i = 0; i < 3; i++) {
            if (str.charAt(i) == 'x') {
                value += 10;
            } else {
                value += Character.getNumericValue(str.charAt(i));
            }
        }

        return value;
    }

    /**
     * Ersetzt alle sondernzeichen mit den entsprechenden Ziffern
     *
     * @param strArray die bowling line
     * @return die vereinfachte bowling line
     */
    private String validateScore(String[] strArray) {
        StringBuilder sb = new StringBuilder();

        for (String str : strArray) {
            char[] charArray = str.toCharArray();
            for (int j = 0; j < charArray.length; j++) {
                if (charArray[j] == '-') {
                    charArray[j] = '0';
                } else if (charArray[j] == '/') {
                    final int RADIX = 10;
                    charArray[j] = Character.forDigit(10 - Character.getNumericValue(charArray[j - 1]), RADIX);
                }
                sb.append(String.valueOf(charArray[j]));
            }
        }

        return sb.toString();
    }

    /**
     * Ueberprueft die bowling line auf die Richtigkeit
     * Beispiel: 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/ 5/5
     * Beispiel: x x x x x x x x x x x x
     * Beispiel: 63 7- x 53 x 5/ 72 2- x 5/x
     *
     * @param stringArray die bowling line
     * @return true, wenn die line vollstaendig und korrekt ist
     */
    private boolean isValidInputFormat(String[] stringArray) {

        if (stringArray.length == 0 || stringArray.length > 12) {
            return false;
        }

        for (int i = 0; i < stringArray.length; i++) {
            if (stringArray[i].length() == 1 && !"x".equals(stringArray[i])) {
                return false;
            } else if (stringArray[i].length() == 2) {
                if (stringArray[i].contains("x")) {
                    return false;
                }
                if (!checkIfDigit(stringArray[i])) {
                    return false;
                }
            } else if (stringArray[i].length() == 3) {
                if (i != 9) {
                    return false;
                }
                if (!checkIfDigit(stringArray[i])) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Ueberprueft ob der String numerische Zeichen enthaelt "-" "/" "x" sind erlaubt
     *
     * @param str der String, der ueberprueft werden soll
     * @return true, wenn alle Zeichen numerisch sind
     */
    private boolean checkIfDigit(String str) {
        str = str.replaceAll("-", "");
        str = str.replaceAll("/", "");
        str = str.replaceAll("x", "");

        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        String gameRules = "x = strike, / = spare, - = miss";
        System.out.println(gameRules);

        // String myRolls = "x x x";
        // String myRolls = "x 5/ x";
        // String myRolls = "53 71";
        String myRolls = "63 7- x 53 x 5/ 72 2- x 5/x";
        BowlingGameMainClass bowling = new BowlingGameMainClass();
        System.out.println("Your rolls: \"" + myRolls + "\"\nYour score: " + bowling.computeScore(myRolls));
        bowling.printFrameScore();
    }
}
